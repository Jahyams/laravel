<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() {
        return view('page.form');
    }
    public function kirim(Request $request){
        $nama = $request->nama;
        return view('page.selamat', compact('nama'));  

    }
}
